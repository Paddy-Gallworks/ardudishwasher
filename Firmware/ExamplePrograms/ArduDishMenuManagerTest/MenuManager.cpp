#include "MenuManager.h"

MenuManager::MenuManager() {
  //Construct the blank menu here!
  for(int i = 0;i<MENU_SIZE;i++)
  {
    functionDescriptor[i] = "BLANK" + (String)i;
  }
  lcd.begin(CHAR_COUNT,LINE_COUNT);
}

void MenuManager::updateScreen() {
  //Draw the current screen
  if(inMenu) {
    lcd.clear();
      //First Line
    lcd.setCursor(1,0);
    lcd.print(functionDescriptor[menuIndex]);
    Serial.println(functionDescriptor[menuIndex]);
      //Second Line
    lcd.setCursor(1,1);
    lcd.print(functionDescriptor[menuIndex+1]);
    Serial.println(functionDescriptor[menuIndex+1]);
      //Draw Cursor
    lcd.setCursor(0,cursorPosition);
    lcd.print("*");
  }
  else {
    runTime++;
    lcd.clear();
    lcd.setCursor(1,0);
    lcd.print("Running: " + currentFunction);
    lcd.setCursor(1,1);
    lcd.print(runTime);
  }
}

void MenuManager::handleNavigationEvent(int eventFlag) {
  //Update the indeces based on an eventFlag
  Serial.print("Event: ");
  Serial.println(eventFlag);
  //Move Cursor Logic
  if(eventFlag == encoderUp) {
    if(cursorPosition == 0) {
      cursorPosition++;
    }
    else {
      if(menuIndex == MENU_SIZE-2) {
        menuIndex = 0;
      }
      else {
        menuIndex++;
      }
      cursorPosition = 0;
    }
  }
  else if(eventFlag == encoderDown) {
    if(cursorPosition == LINE_COUNT-1) {
      cursorPosition--;
    }
    else {
      if(menuIndex == 0) {
        menuIndex = MENU_SIZE-2;
        cursorPosition = LINE_COUNT-1;
      }
      else {
        menuIndex--;
        cursorPosition = LINE_COUNT-1;
      }
    }
  }
  //Run Function Logic
  else if(eventFlag == btnEncodeOK) {
    Serial.print("Running: ");
    Serial.println(functionDescriptor[cursorPosition+menuIndex]);
    currentFunction = functionDescriptor[cursorPosition+menuIndex];
    inMenu = false;
    currentFunction = functionDescriptor[cursorPosition + menuIndex];
    menuFunc[cursorPosition + menuIndex](0);
    inMenu = true;
  }
  updateScreen();
  Serial.print("Menu Index: ");
  Serial.println(menuIndex);
  Serial.print("Curosr Index: ");
  Serial.println(cursorPosition);
}

void MenuManager::addMenuItem(uint8_t index,String text,void newMenuFunction(int)) {
  functionDescriptor[index] = text;
  menuFunc[index] = newMenuFunction;
}

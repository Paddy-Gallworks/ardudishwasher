#ifndef STEPMOTOR_H
#define STEPMOTOR_H

#include <Arduino.h>
#include <stdlib.h>
#include "MotorDirection.h"
//Class used to control a step motor through a AXXXX Controller.


class StepMotor
{
protected:
	//Pins
	uint8_t stepPin;
	uint8_t dirPin;
	uint8_t enablePin;
	//State
	MotorDirection direction; //direction that the step motor is turning -> not the axis travel direction!!
public:
	StepMotor()
	{
		
	}
	void begin(uint8_t _step, uint8_t _dir, uint8_t _enable); //mimics the full constructor
	virtual void step();
	//Set State
	void setDirDirect(MotorDirection); //Dangerous!!	
	void disable();
	void enable();
};

#endif
#ifndef MOTORDIRECTION_H
#define MOTORDIRECTION_H

enum MotorDirection
{
	POSITIVE,
	NEGATIVE
};
#endif
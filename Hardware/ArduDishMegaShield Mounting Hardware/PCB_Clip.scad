$fn = 30;
//Variables
BOLT_SPACING = 25;
CLIP_SETBACK = 12;
WALL = 3;
PCB_CLEARANCE = 3;
EDGE_CLEARANCE = 5;
TOLERANCE = 0.5;
CLIP_OVERLAP = 3;
INSERT_RADIUS = 1.65;
INSERT_HEIGHT = 10;

//Conditionals
CLIP_HEIGHT = CLIP_SETBACK-TOLERANCE;
//Model
difference() {
  union() {
    cube([PCB_CLEARANCE + EDGE_CLEARANCE - TOLERANCE,BOLT_SPACING+(2*WALL),CLIP_HEIGHT]);
    translate([-CLIP_OVERLAP,0,CLIP_HEIGHT-0.01])
      cube([CLIP_OVERLAP+PCB_CLEARANCE + EDGE_CLEARANCE - TOLERANCE,BOLT_SPACING+(2*WALL),WALL]);
  }
  translate([PCB_CLEARANCE-TOLERANCE,WALL,-0.01]) {
    cylinder(r=INSERT_RADIUS,h=INSERT_HEIGHT);
    translate([0,BOLT_SPACING,0])
      cylinder(r=INSERT_RADIUS,h=INSERT_HEIGHT);
  }
}

/*  ARDUDISH Project
  AMMBCB Mounting Hardware
  This model is intended to mount the AMMBCB control board to the front of a dishwasher. A whole needs to be cut into the plate steel on the front of the machine, where this mounting component is then installed.
*/

//Variables
$fn = 30;
CORNER_RADIUS = 3;
CLIP_WALL = 3;
CLIP_SUPPORT = 10;
CLIP_SETBACK = 10;
DW_HEIGHT = 90;
DW_WIDTH = 150;
DW_OVERLAP = 10;
DW_THICKNESS = 4;
DW_OVERLAP_THICKNESS = 3;
DW_BOLT_CLEARANCE = 30;
DW_BOLT_DIAMETER = 4.5;
MP_ANGLE = -15;
MP_WIDTH = 135;
MP_HEIGHT = 60;
MP_THICKNESS = 3;
MP_CLIP_OFFSET = 10;
MP_LIP = 2;
BOARD_CUTOUT_HOZ = 10;
BOARD_CUTOUT_VERT = 2;
PCB_THICKNESS = 1.6;
TAPER_LENGTH = 55;
WALL = 4.5;


//Conditionals
TAPER_WIDTH_OFFSET = (DW_WIDTH - MP_WIDTH)/2;
TAPER_HEIGHT_OFFSET = (DW_HEIGHT - MP_HEIGHT)/2;
OVERLAP_WIDTH = DW_WIDTH + (2 * DW_OVERLAP);
OVERLAP_HEIGHT = DW_HEIGHT + (2 * DW_OVERLAP);
CLIP_DEPTH = CLIP_SETBACK+PCB_THICKNESS+CLIP_WALL/2;

//Modules
module ElecFreaksLCDBoard_Simplified() {
  color("crimson") {
    union() {
      cube([97.4,54.4,1.6]); //Main PCB
      translate([0.8,11,0])
        cube([80,36,5]); //LCD PCB
      translate([5.8,16,0])
        cube([75,25.4,12.5]); //LCD Metal Frame
      translate([22.5,1.7,0])
        cube([20,6.5,10.5]); //Expansion Pins
      translate([84,14,0])
        cube([10.5,10.5,4.2]); //Key Base
      translate([89,19,0])
        cylinder(r=3,h=14); //Key stalk
      translate([89,38.4,0])
        cylinder(r=6,h=25); //Encoder stalk
    }
  }
}
module MountingPlate() {
  union() {
    color("violet") {
      difference() {
        translate([MP_LIP,MP_LIP,0])
        minkowski() //Main Plate
        {
          cylinder(r=CORNER_RADIUS,h=0.001);
          cube([MP_WIDTH-(2*MP_LIP),MP_HEIGHT-(2*MP_LIP),MP_THICKNESS]);
        }
        translate([BOARD_CUTOUT_HOZ,BOARD_CUTOUT_VERT,-0.1]) //Holes for encoder / LCD / Expansion Pins
        {
          translate([6,16,0])
            cube([75,25.4,12.5]); //LCD Metal Frame
          translate([22,2.0,0])
            cube([22,8,10.5]); //Expansion Pins
          translate([89,19,0])
            cylinder(r=3,h=14); //Key stalk
          translate([89,37.4,0])
            cylinder(r=6,h=25); //Encoder stalk
          translate([-3,15,-0.01]) {//Holes for PCB Mount
            cylinder(r=1.6,h=20);
            translate([0,25,0])
              cylinder(r=1.6,h=20);
          }
          translate([4,50.2,-0.01]) //Hole for Screen Contrast
            cylinder(r=4,h=20);
        }
      }
    }
    mirror([0,0,1])
    color("RoyalBlue") {// Clips and Pads, PCB support.
      translate([BOARD_CUTOUT_HOZ,BOARD_CUTOUT_VERT,-0.1]) {
        translate([-CLIP_WALL,-CLIP_WALL,0]) { //Lower Left Support
          cube([CLIP_WALL,10,CLIP_DEPTH]); //Vertical
          cube([15,CLIP_WALL,CLIP_DEPTH]); //Horizontal
        }
        cube([15,8,CLIP_SETBACK]); //Lower Left Pad
        translate([-CLIP_WALL,54.6,0]) { //Upper Left hand Support
          mirror([0,1,0]) cube([CLIP_WALL,5,CLIP_DEPTH]); //Vertical
          cube([8,CLIP_WALL,CLIP_DEPTH]); //Horizontal
          translate([CLIP_WALL,-14,0]) //Upper Left Hand Pad
            cube([6,4,CLIP_SETBACK-3.6]);
        }
        translate([97.4,0,0]) { // Right Hand Clip
          difference() {
            translate([-1,0,0])
              cube([3+CLIP_WALL,54.4,CLIP_DEPTH]);
            translate([-1.01,-0.01,CLIP_SETBACK])
              cube([1.1,54.42,PCB_THICKNESS]);
          }
        }

      }
    }
  }
}
module AMMBCB() {
  color("firebrick")
  {
    cube([106,57.5,12]); //Arduino PCB and Plastic Shield
    translate([8,0,0])
    cube([100,57.5,15.5]); //Arduino Headers
    translate([8,0,0])
    cube([68,57.5,31.5]);
  }
}
module FlangeTaper() {
  union() {
    //Taper
    difference()
    {
      hull() {
        translate([DW_OVERLAP,DW_OVERLAP,0])
        {
          minkowski() {
            cylinder(r=CORNER_RADIUS,h=0.001);
            cube([DW_WIDTH,DW_HEIGHT,DW_THICKNESS]);
          }
          rotate([MP_ANGLE,0,0])
          translate([TAPER_WIDTH_OFFSET,TAPER_HEIGHT_OFFSET,TAPER_LENGTH])
          minkowski() {
            cylinder(r=CORNER_RADIUS,h=0.001);
            cube([MP_WIDTH,MP_HEIGHT,MP_THICKNESS]);
          }
        }
      }
      translate([WALL,WALL,-0.01])
      hull() {
        translate([DW_OVERLAP,DW_OVERLAP,0])
        {
          minkowski() {
            cylinder(r=CORNER_RADIUS,h=0.001);
            cube([DW_WIDTH-(2*WALL),DW_HEIGHT-(2*WALL),DW_THICKNESS]);
          }
          rotate([MP_ANGLE,0,0])
          translate([TAPER_WIDTH_OFFSET,TAPER_HEIGHT_OFFSET,TAPER_LENGTH])
          minkowski() {
            cylinder(r=CORNER_RADIUS,h=0.001);
            cube([MP_WIDTH-(2*WALL),MP_HEIGHT-(2*WALL),MP_THICKNESS]);
          }
        }
      }
    }
    //Flange and Bolt Pattern
    difference() {
      minkowski() {
        cylinder(r=CORNER_RADIUS,h=0.001);
        cube([OVERLAP_WIDTH,OVERLAP_HEIGHT,DW_OVERLAP_THICKNESS]);
      }
      translate([DW_OVERLAP+WALL,DW_OVERLAP+WALL,-0.01])
      minkowski() {
        cylinder(r=CORNER_RADIUS,h=0.001);
        cube([DW_WIDTH-(2*WALL),DW_HEIGHT-(2*WALL),DW_OVERLAP_THICKNESS+0.02]);
      }
      translate([DW_BOLT_CLEARANCE,2,-0.01])
      cylinder(r=DW_BOLT_DIAMETER/2,h=20);
      translate([OVERLAP_WIDTH-DW_BOLT_CLEARANCE,2,-0.01])
      cylinder(r=DW_BOLT_DIAMETER/2,h=20);
      translate([DW_BOLT_CLEARANCE,OVERLAP_HEIGHT-2,-0.01])
      cylinder(r=DW_BOLT_DIAMETER/2,h=20);
      translate([OVERLAP_WIDTH-DW_BOLT_CLEARANCE,OVERLAP_HEIGHT-2,-0.01])
      cylinder(r=DW_BOLT_DIAMETER/2,h=20);
    }
  }
}
module complete() {
  union() {
    color("grey")
    FlangeTaper();
    rotate([MP_ANGLE,0,0])
    translate([DW_OVERLAP+TAPER_WIDTH_OFFSET,DW_OVERLAP+TAPER_HEIGHT_OFFSET,TAPER_LENGTH+1])
    color("lightgrey")
    MountingPlate();
    rotate([MP_ANGLE,0,0])
    translate([30,26,44.5])
    {
      #ElecFreaksLCDBoard_Simplified();
      translate([0,0,-31.5])
      AMMBCB();
    }
  }
}
module fullPrintExport() {
  union() {
    color("grey")
    FlangeTaper();
    rotate([MP_ANGLE,0,0])
    translate([DW_OVERLAP+TAPER_WIDTH_OFFSET,DW_OVERLAP+TAPER_HEIGHT_OFFSET,TAPER_LENGTH+1])
    color("lightgrey")
    MountingPlate();
  }
}
module twoPartPrintExport() {
  difference() {
    FlangeTaper();
      rotate([MP_ANGLE,0,0])
      translate([DW_OVERLAP+TAPER_WIDTH_OFFSET-0.3,DW_OVERLAP+TAPER_HEIGHT_OFFSET-0.7,TAPER_LENGTH+1])
      scale([1.01,1.01,3])
      MountingPlate();
  }
}

//complete();
//fullPrintExport();
MountingPlate();
//twoPartPrintExport();

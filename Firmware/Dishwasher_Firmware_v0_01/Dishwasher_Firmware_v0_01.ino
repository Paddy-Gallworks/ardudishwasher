//Dish Washer Firmware v0 01
#include <Wire.h>
#include <LiquidCrystal.h>
#include <TimerOne.h>
#include "Configuration.h"
#include "MenuManager.h" //Only used by ELECFREAKS_LCD_KEY_MODULE
#ifdef ELECFREAKS_LCD_KEY_MODULE
	LiquidCrystal lcd(8,9,4,5,6,7); //Pin definitions fixed from PCB
	uint16_t adc_key_in  = 0;
	uint8_t eventFlag = 0;
	uint8_t encoderFlag = 0;
	MenuManager menu = MenuManager();
#endif //ELECFREAKS_LCD_KEY_MODULE
void setup() {
  Serial.begin(9600);
	Timer1.initialize(TIMER_TICK); // See behavioural configuration
	Timer1.attachInterrupt(timerTick); //Add service function here.
  /* -User Outputs ----------------------------------------------------- */
  #ifdef ELECFREAKS_LCD_KEY_MODULE
    /* LCD Module */
    lcd.begin(16,2);
		pinMode(10,OUTPUT); //LCD Backlight Pin
		analogWrite(10,15); //Backlight Value
    lcd.setCursor(0,0);
    lcd.print("ARDUDISH");
    lcd.setCursor(0,1);
    lcd.print("starting...");
    /* Encoder and Toggle */
    pinMode(Encoder_A, INPUT_PULLUP);
    pinMode(Encoder_B, INPUT_PULLUP);
    attachInterrupt(1, encoder_Update, FALLING); // 1 is interupt on Encoder_A
  #endif //ELECFREAKS_LCD_KEY_MODULE
	//Setup Menu Displays
	menu.tickerDisplay("Starting...");
	menu.screenDisplayMode = startUp;
	consoleLog("Starting...");
  pinMode(9,OUTPUT); //Check what this is for!!?
  /* -User Inputs ------------------------------------------------------ */
  #ifdef BUZZ_PIN
    pinMode(BUZZ_PIN,OUTPUT);
  #endif
  #ifdef KILL_PIN
    pinMode(KILL_PIN,INPUT_PULLUP);
  #endif
  /* -Sensor Inputs ---------------------------------------------------- */
  #ifdef FLOW_METER_WATER_LEVEL_SENSOR
    pinMode(FLOW_METER_PIN,INPUT_PULLUP);
  #endif
  pinMode(OVER_FLOW_PRESSURE_SWITCH_PIN,INPUT_PULLUP);
  /* -Relay Outputs ---------------------------------------------------- */
  pinMode(INLET_VALVE_ONE_PIN,OUTPUT);
  #ifdef SECOND_INLET_VALVE
    pinMode(INLET_VALVE_TWO_PIN,OUTPUT);
  #endif
  #ifdef STEP_MOTOR_DISPENSER
    dispenserMotor.begin(zStepPin,zDirPin,zEnablePin);
    dispenserMotor.disable();
  #endif
  #ifdef SOLENOID_DISPENSER
    pinMode(DISPENSER_PIN,OUTPUT);
  #endif
  pinMode(WASH_MOTOR_PIN,OUTPUT);
  pinMode(DRAIN_MOTOR_PIN,OUTPUT);
  pinMode(HEATER_PIN,OUTPUT);
	menu.tickerDisplay("Building Menu");
	consoleLog("Building Menu");
	#ifdef ELECFREAKS_LCD_KEY_MODULE
	//Define Menu Structure
		menu.addMenuItem(0,"Std Wash",[]
			{
				menu.screenDisplayMode = infoScreen;
				menu.currentFunction = "Std Wash";
				menu.updateScreen();
				//Set Run Flag to Yes
				consoleLog("Std Wash Start");
				standardFullCycle();
				menu.screenDisplayMode = mainMenu;
				//Set Run Flag to No
			});
		menu.addMenuItem(1,"Drain",drainTank);
		menu.addMenuItem(2,"Sys Test",systemTest);
		menu.addMenuItem(3,"Heat Test",heaterTest);
		menu.addMenuItem(4,"Temp Test",temperatureTest);
    menu.addMenuItem(5,"Press Test",pressureTest);
    menu.addMenuItem(6,"Rel Test",releaseDetergent);
		menu.addMenuItem(7,"Self Clean",selfClean);
		menu.addMenuItem(8,"OVR Fill",fillTank);
		menu.updateScreen();
	#endif
	//SD Card
	menu.tickerDisplay("SD Start");
	consoleLog("SD Start");
	//EEPROM
	menu.tickerDisplay("EEPROM Start");
	consoleLog("EEPROM Start");
	//RTC
	menu.tickerDisplay("RTC Start");
	consoleLog("RTC Start");

	//Check EEPROM Run log
	byte runFlag = eepromRead(RUN_LOG_EEPROM_LOCATION);
	if(runFlag == (byte)1); {// Unfinished Run Detected
		consoleLog("Incomplete Cycle");
		menu.tickerDisplay("Incomplete Cycle");
	}
	else { // No Unfinished Run Detected
		consoleLog("Clean Start");
		menu.tickerDisplay("Clean Start");
	}
	//Reset Menu MODEL
	menu.screenDisplayMode = mainMenu;
}
void loop() {
  #ifdef ELECFREAKS_LCD_KEY_MODULE
		eventFlag = read_LCD_buttons();
		if(eventFlag != btnNone) {
			menu.handleNavigationEvent(eventFlag);
			eventFlag = btnNone;
		}
		if(encoderFlag != btnNone) {
			menu.handleNavigationEvent(encoderFlag);
			encoderFlag = btnNone;
		}
  #endif //ELECFREAKS_LCD_KEY_MODULE
}
//Non CSM Action Functions
void standardFullCycle() {
	consoleLog("Std Wash Starting");
	consoleLog("Start Rinse Phase");
	#ifdef STARTING_SMART_RINSE_V1
		smartRinseCycle();
	#else
		standardRinseCycle();
	#endif
	consoleLog("Start Wash Phase");
	releaseDetergent();
	fillTank();
	runWashMotor(washTime);
	drainTank();
	consoleLog("Start Rinse Phase");
	smartRinseCycle();
	standardRinseCycle();
	consoleLog("End Rinse Phase");
	consoleLog("Start Dry Phase");
	fillTank();
	heatDry(dryTime/2);
	delay(100);
	runWashMotor(rinseWashMotorRuntime/2);
	delay(100);
	heatDry(dryTime/2);
	drainTank();
	consoleLog("End Drying Phase");
	disableAll();
}

void selfClean() {
	#ifdef ELECFREAKS_LCD_KEY_MODULE
		lcd.clear();
		lcd.setCursor(0,1);
		lcd.print("Self Clean");
	#endif
	fillTank();
	runWashMotor(300000); //5 minute clear Cycle
	drainTank();
	fillTank();
	runWashMotor(100000); //100 second clear Cycle
	drainTank();
}
//Cycle SubTasks
void standardRinseCycle() {
	consoleLog("Begin Rinse");
	delay(STANDARD_ACTION_DELAY);
	//Fill the Tank
	fillTank();
	//Run Wash Motor
	runWashMotor(rinseWashMotorRuntime);
	consoleLog("Drip Dry");
	bigDelay(10000); //10 Second drip dry.
	//Run Drain Pump
	drainTank();
	consoleLog("End Rinse");
}
void smartRinseCycle(){
	consoleLog("Begin Smart Rinse");
  delay(STANDARD_ACTION_DELAY);
  fillTank();
  runWashMotor(rinseWashMotorRuntime/3);
	consoleLog("Smart Soak");
  bigDelay(rinseWashMotorRuntime/3);
  runWashMotor(rinseWashMotorRuntime/3);
	consoleLog("Drip Dry");
  bigDelay(10000);
  drainTank();
	consoleLog("End Smart Rinse");
}
void fillTank() {
	consoleLog("Filling Tank");
	#ifdef FLOW_METER_WATER_LEVEL_SENSOR
		int count = 0;
		digitalWrite(INLET_VALVE_ONE_PIN,HIGH);
		while(count < fillFlowMeterCount && digitalRead(OVER_FLOW_PRESSURE_SWITCH_PIN) == HIGH) {
			if(digitalRead(FLOW_METER_PIN) == LOW) {
				digitalWrite(9,!digitalRead(9));
				while(digitalRead(FLOW_METER_PIN) == LOW) {}
				count ++;
			}
		}
		digitalWrite(9,LOW);
		digitalWrite(INLET_VALVE_ONE_PIN,LOW);
	#endif
	#ifdef OVER_FLOW_WATER_LEVEL_SENSOR
		delay(STANDARD_ACTION_DELAY);
		digitalWrite(INLET_VALVE_ONE_PIN,HIGH);
		while(digitalRead(OVER_FLOW_PRESSURE_SWITCH_PIN) == LOW){
			//Wait for tank to fill
    }
		consoleLog("Drain Back");
		digitalWrite(INLET_VALVE_ONE_PIN,LOW);
		delay(STANDARD_ACTION_DELAY);
		digitalWrite(DRAIN_MOTOR_PIN, HIGH);
		delay(DRAIN_BACK);
		digitalWrite(DRAIN_MOTOR_PIN, LOW);
		delay(STANDARD_ACTION_DELAY);
	#endif
	consoleLog("Tank Full");
}
void drainTank() {
	consoleLog("Draining Tank");
	delay(STANDARD_ACTION_DELAY);
	digitalWrite(DRAIN_MOTOR_PIN,HIGH);
	bigDelay(rinseDrainMotorRuntime);
	digitalWrite(DRAIN_MOTOR_PIN,LOW);
	delay(STANDARD_ACTION_DELAY);
}
void releaseDetergent() {
	consoleLog("Release Detergent");
	delay(STANDARD_ACTION_DELAY);
	#ifdef SOLENOID_DISPENSER
		digitalWrite(DISPENSER_PIN,HIGH);
		delay(DETERGENT_RELEASE_TIME);
		digitalWrite(DISPENSER_PIN,LOW);
	#endif
	#ifdef STEP_MOTOR_DISPENSER
		dispenserMotor.enable();
		delay(1000);
		dispenserMotor.setDirDirect(defaultDispenserMotorDirection);
		for(int i = 0; i < dispenserMotorStepCount; i++)
		{
			//Serial.println("STEP");
			dispenserMotor.step();
			delay(defaultStepDelay);
		}
		delay(2000);
		dispenserMotor.setDirDirect((defaultDispenserMotorDirection+1)%2);
		for(int i = 0; i < dispenserMotorStepCount; i++)
		{
			//Serial.println("STEP");
			dispenserMotor.step();
			delay(defaultStepDelay);
		}
		delay(1000);
		dispenserMotor.disable();
	#endif
	delay(STANDARD_ACTION_DELAY);
	consoleLog("End Release");
}
void runWashMotor(unsigned long runTime) {
	consoleLog("Wash Motor On");
	delay(STANDARD_ACTION_DELAY);
  digitalWrite(WASH_MOTOR_PIN,HIGH);
	bigDelay(runTime);
	digitalWrite(WASH_MOTOR_PIN,LOW);
	delay(STANDARD_ACTION_DELAY);
	consoleLog("Wash Motor Off");
}
void heatDry(unsigned long runTime) {
	consoleLog("Heater On");
	#ifdef SLOW_PWM_HEATER
		unsigned long endTime = millis()+runTime;
		unsigned long dutyTimer = 0;
		while(millis() < endTime) {
			dutyTimer = millis();
			while(dutyTimer + heatOnCycle > millis()) {
				digitalWrite(HEATER_PIN, HIGH);
			}
			dutyTimer = millis();
			while(dutyTimer + heatOffCycle > millis()) {
				digitalWrite(HEATER_PIN, LOW);
			}
		}
	#endif
	#ifdef SELF_REGULATING_HEATER
		digitalWrite(HEATER_PIN,HIGH);
		bigDelay(runTime);
		digitalWrite(HEATER_PIN,LOW);
	#endif
}
void bigDelay(unsigned long time) {
	#ifdef DIAGNOSTIC_MODE
		delay(2000);
	#else
		unsigned long endTime = millis() + time;
		while(millis() < endTime) {
			#ifndef OVER_FLOW_WATER_LEVEL_SENSOR
			if(digitalRead(OVER_FLOW_PRESSURE_SWITCH_PIN) == LOW)
			{
				digitalWrite(INLET_VALVE_ONE_PIN,LOW);
				drainTank();
			}
			#endif
		}
	#endif
}
//System Test Code
void systemTest() {
	#ifdef ELECFREAKS_LCD_KEY_MODULE
		lcd.clear();
		lcd.setCursor(0,0);
		lcd.print("SYSTEM TEST");
		lcd.setCursor(0,1);
		lcd.print("Starting");
		delay(500);
		//Overflow Test
		lcd.clear();
		lcd.setCursor(0,0);
		lcd.print("SYSTEM TEST");
		lcd.setCursor(0,1);
		lcd.print("OverFlow: ");
		if(digitalRead(OVER_FLOW_PRESSURE_SWITCH_PIN)) {
			lcd.print("HIGH");
		}
		else {
			lcd.print("LOW");
		}
		//Inlet Valve 1
		lcd.clear();
		lcd.setCursor(0,0);
		lcd.print("System Test");
		lcd.setCursor(0,1);
		lcd.print("Inlet 1: HIGH");
		digitalWrite(INLET_VALVE_ONE_PIN,HIGH);
		delay(1000);
		digitalWrite(INLET_VALVE_ONE_PIN,LOW);
		lcd.setCursor(0,1);
		lcd.print("Inlet 1: LOW ");
		delay(500);
		//Inlet Valve 2
		lcd.clear();
		lcd.setCursor(0,0);
		lcd.print("System Test");
		lcd.setCursor(0,1);
		lcd.print("Inlet 2: HIGH");
		digitalWrite(INLET_VALVE_TWO_PIN,HIGH);
		delay(1000);
		digitalWrite(INLET_VALVE_TWO_PIN,LOW);
		lcd.setCursor(0,1);
		lcd.print("Inlet 2: LOW ");
		delay(500);
		//Detergent release
		lcd.clear();
		lcd.setCursor(0,0);
		lcd.print("System Test");
		lcd.setCursor(0,1);
		lcd.print("Releasing Soap");
		releaseDetergent();
		//Wash Motor
		lcd.clear();
		lcd.setCursor(0,0);
		lcd.print("System Test");
		lcd.setCursor(0,1);
		lcd.print("Wash Motor Run");
		delay(100);
		digitalWrite(WASH_MOTOR_PIN,HIGH);
		delay(1000);
		digitalWrite(WASH_MOTOR_PIN,LOW);
		//Drain Motor
		lcd.clear();
		lcd.setCursor(0,0);
		lcd.print("System Test");
		lcd.setCursor(0,1);
		lcd.print("Drain Motor Run");
		delay(500);
		digitalWrite(DRAIN_MOTOR_PIN,HIGH);
		delay(1000);
		digitalWrite(DRAIN_MOTOR_PIN,LOW);
		//Heater
		lcd.clear();
		lcd.setCursor(0,0);
		lcd.print("System Test");
		lcd.setCursor(0,1);
		lcd.print("Heater:");
		delay(500);
		digitalWrite(HEATER_PIN,HIGH);
		for(int timer = 0; timer<6;timer++)
		{
			lcd.setCursor(10,1);
			lcd.print("    ");
			lcd.setCursor(10,1);
			lcd.print(analogRead(TEMP_SENSOR_PIN));
			delay(500);
		}
		digitalWrite(HEATER_PIN,LOW);
	#endif
}
void heaterTest() {
	menu.screenDisplayMode = infoScreen;
	menu.currentFunction = "Heater Test";
	digitalWrite(HEATER_PIN,HIGH);
	menu.tickerDisplay("Heater On");
	for(int timer = 0; timer<20;timer++)
	{
		int rawADC = analogRead(TEMP_SENSOR_PIN);
		double testFilter = 0.95*rawADC - 90;
		String displayMessage;
		displayMessage += (int)testFilter;
		displayMessage += " Deg C";
		consoleLog(displayMessage);
		delay(500);
	}
	digitalWrite(HEATER_PIN,LOW);
	menu.tickerDisplay("Heater Off");
}
void temperatureTest() {
	menu.screenDisplayMode = infoScreen;
	menu.currentFunction = "Temp Test";
	for(int timer = 0; timer<20;timer++)
	{
		int rawADC = analogRead(TEMP_SENSOR_PIN);
		double testFilter = 0.95*rawADC - 90;
		String displayMessage;
		displayMessage += (int)testFilter;
		displayMessage += " Deg C";
		consoleLog(displayMessage);
		delay(500);
	}
}
void pressureTest() {
	menu.screenDisplayMode = infoScreen;
	menu.currentFunction = "Pressure Test";
	for(int timer = 0; timer<20;timer++)
	{
		String dataString = "Status: ";
		dataString += (int)digitalRead(OVER_FLOW_PRESSURE_SWITCH_PIN);
		consoleLog(dataString);
		delay(500);
	}
}

//------------------------------------------------------------------------
// * IO CODE
//------------------------------------------------------------------------
// * timerTick: Available for future addition of hardware timer.
void timerTick(){

}
// * encoder_Update: Manages the hardware interrupt provided by the encoder.
void encoder_Update() {
  if(digitalRead(Encoder_B)) {
		encoderFlag = encoderUp;
  }
  else {
		encoderFlag = encoderDown;
  }
}
// * read_LCD_Buttons: reads LCD shield keys in a polling fashion.
int read_LCD_buttons() {
  uint16_t adc_key_in = analogRead(0);
  if (adc_key_in > 1000) return btnNone;
  if (adc_key_in < 50)   return btnLeft;
  if (adc_key_in < 150)  return btnUp;
  if (adc_key_in < 250)  return btnRight;
  if (adc_key_in < 450)  return btnSelect;
  if (adc_key_in < 650)  return btnDown; //originally 700
  if (adc_key_in < 850)  return btnEncodeOK;
  return btnNone;  // when all others fail, return this...
}
void disableAll() {
  #ifdef BUZZ_PIN
    digitalWrite(BUZZ_PIN,LOW);
  #endif
  digitalWrite(INLET_VALVE_ONE_PIN,LOW);
  #ifdef SOLENOID_DISPENSER
    digitalWrite(DISPENSER_PIN,LOW);
  #endif
  digitalWrite(WASH_MOTOR_PIN,LOW);
  digitalWrite(DRAIN_MOTOR_PIN,LOW);
  digitalWrite(HEATER_PIN,LOW);
}
void clearFirstRow() {
	lcd.setCursor(0,0);
	lcd.print("                ");
}
void clearSecondRow() {
	lcd.setCursor(0,1);
	lcd.print("                ");
}
void consoleLog(String message) {
	Serial.print(millis());
	Serial.print(": ");
	Serial.println(message);
	Serial.flush();
	menu.tickerDisplay(message);
	menu.updateScreen();
}
void eepromWrite(uint8_t _address,uint8_t _data) {
	Wire.beginTransmission(160); // (0xA0)
	Wire.write(_address);
	Wire.write(_data);
	Wire.endTransmission();
}
void eepromRead(uint8_t _address) {
	byte rdata = 0xFF;
	Wire.beginTransmission(160);
	Wire.write(_address);
	Wire.endTransmission();
	Wire.requestFrom(160,1);
	if (Wire.available())
		rdata = Wire.read();
	return rdata;
}

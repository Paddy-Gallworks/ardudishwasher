//Arduino Dishwasher Control Board Configuration

//Demonstration Mode
//#define DEMO_MODE

#ifndef CONFIGURATION_H
#define CONFIGURATION_H
enum navigationEvents {
  btnRight, //0
  btnUp, //1
  btnDown, //2
  btnLeft, //3
  btnSelect, //4
  btnEncodeOK, //5
  encoderUp, //6
  encoderDown, //7
  btnNone //8
};
enum screenDisplayModes {
  mainMenu, //0 - Relies on the operation of the menu manager to populate a menu.
  infoScreen, //1 - Screen that displays by default during a cycle.
  simpleFunction, //2 - Screen that display for simple functions, like drain etc.
  startUp //3 - Screen during boot of the dishwasher
};



//----------------------------------------------------------------------------------------------------
// DISHWASHER MODELS - Selection
//----------------------------------------------------------------------------------------------------

/* Uncomment the correct dishwasher model from the list below.
If the model is not yet supported or contains non standard mechanical parts,
select CUSTOM_MODEL */

#define DANBY_1801MWP
//#define CUSTOM_MODEL

//
// DISHWASHER MODEL - Conditionals
//----------------------------------------------------------------------------------------------------

//#define DIAGNOSTIC_MODE

#ifdef DANBY_1801MWP
	#define SOLENOID_DISPENSER
	#define SELF_REGULATING_HEATER
	//#define FLOW_METER_WATER_LEVEL_SENSOR //Primary Fill Sensor
  #define OVER_FLOW_WATER_LEVEL_SENSOR //Used as a backup
  #define SECOND_INLET_VALVE
  #define WATER_TEMP_SENSOR
#endif

//Use the following section to define your custom dishwasher hardware.
#ifdef CUSTOM_MODEL
	/* Dispensers*/
	//#define SOLENOID_DISPENSER
	/* Heaters*/
	//#define SLOW_PWM_HEATER
	#define SELF_REGULATING_HEATER //Controller does not need to monitor temperature.
	/* Water level Sensing*/
	//#define FLOAT_WATER_LEVEL_SENSOR
	//#define TIMER_WATER_LEVEL_SENSOR
	//#define FLOW_METER_WATER_LEVEL_SENSOR
	#define OVER_FLOW_WATER_LEVEL_SENSOR //Only an overflow water level sensor is installed.
#endif

//----------------------------------------------------------------------------------------------------
// CONTROL BOARD SELECTION
//----------------------------------------------------------------------------------------------------

/*
	* Uncomment the correct Control Board from the list below. If using a custom control board, define it manually in the Custom Section.
*/

//#define ARDUDISH_CONTROL_BOARD //Coming Soon!
#define ARDUDISH_MODULE_BASED_CONTROLLER_V01
//#define CUSTOM_CONTROL_BOARD

//
// CONTROL BOARD - Conditionals
//----------------------------------------------------------------------------------------------------

#ifdef ARDUDISH_CONTROL_BOARD
	//IN PROGRESS
#endif //ARDUDUSH_CONTROL_BOARD
#ifdef ARDUDISH_MODULE_BASED_CONTROLLER_V01
  /* Relay channel definitions
    Ch1 22: Inlet Valve One
    Ch2 24: Inlet Valve Two
    Ch3 28: Drain Motor
    Ch4 30: Wash Motor
    Ch5 32: Dispenser
    Ch6 34: Heater
    Ch7 36:
    Ch8 38:
  */
	//Relay Pin Definitions
	#define INLET_VALVE_ONE_PIN 22
  #ifdef SECOND_INLET_VALVE
    #define INLET_VALVE_TWO_PIN 24
  #endif
	#define WASH_MOTOR_PIN 30
	#define DRAIN_MOTOR_PIN 28
	#define HEATER_PIN 34
	#ifdef STEP_MOTOR_DISPENSER
		#error Step Motor Dispensor Not Supported
	#endif
	#ifdef SOLENOID_DISPENSER
		#define DISPENSER_PIN 32
	#endif

  /* Sensor Channel definitions
    Ch1 A8: Temperature
    Ch2 A9: Overflow Sensor
    Ch3 A10:Flow Meter
    Ch4 A11:
    Ch5 A12:
  */
	//Sensor Pin Definitions
  #ifdef FLOW_METER_WATER_LEVEL_SENSOR
    #define FLOW_METER_PIN A10
  #endif
	#ifdef OVER_FLOW_WATER_LEVEL_SENSOR
    #define OVER_FLOW_PRESSURE_SWITCH_PIN A9
		#define DRAIN_BACK 5000
	#endif
  #ifdef WATER_TEMP_SENSOR
    #define TEMP_SENSOR_PIN A8
  #endif

	// LCD Pin Definitions
	#define ELECFREAKS_LCD_KEY_MODULE
#endif //ARDUDISH_MODULE_BASED_CONTROLLER_V01
#ifdef CUSTOM_CONTROL_BOARD
	//Relay Definitions
	#define INLET_VALVE_ONE_PIN 6
  #ifdef SECOND_INLET_VALVE
    #error Second Inlet Valve Not Supported
  #endif
	#define INLET_VALVE_TWO_PIN
	#define WASH_MOTOR_PIN 4
	#define DRAIN_MOTOR_PIN 57
	#define HEATER_PIN 5
	//Misc
	#define BUZZ_PIN 37
	#define KILL_PIN 41
	//Sensor Pin Definitions
	#define OVER_FLOW_PRESSURE_SWITCH_PIN 44
	#define FLOW_METER_PIN 42
	#ifdef STEP_MOTOR_DISPENSER
		#include "StepMotor.h"
		#include "MotorDirection.h"
		#define A4988
		//#define TRIMAMIC_STEP_STICK
		//Define Motor Pins
		const uint8_t zStepPin = 26;
		const uint8_t zDirPin = 28;
		const uint8_t zEnablePin = 24;
		MotorDirection defaultDispenserMotorDirection = NEGATIVE; //POSITIVE or NEGATIVE;
		//Set number of steps required to open the door.
		#ifdef A4988
			const uint8_t dispenserMotorStepCount = 55; //Number of steps required to open door successfully.
			const uint8_t defaultStepDelay = 30;
		#endif
		#ifdef TRIMAMIC_STEP_STICK
			const uint8_t dispenserMotorStepCount = 400;
			const uint8_t defaultStepDelay = 30;
		#endif
	#endif
	#ifdef SOLENOID_DISPENSER
		#define DISPENSER_PIN 11
	#endif
	#ifdef OVER_FLOW_WATER_LEVEL_SENSOR
		#define DRAIN_BACK 5000
	#endif
#endif //CUSTON_CONTROL_BOARD

//
// CONTROL BOARD - Sub-module Conditionals
//

#ifdef ELECFREAKS_LCD_KEY_MODULE
	#include <LiquidCrystal.h>
  #define Encoder_A 3
  #define Encoder_B 2
#endif //ELECFREAKS_LCD_KEY_MODULE

//----------------------------------------------------------------------------------------------------
// BEHAVIOURAL PARAMETERS
//----------------------------------------------------------------------------------------------------

//Verbose Serial: tailors how much information is dumped to the serial port / SD
//during operation.
#define VERBOSE_SERIAL

//TIMER_TICK: defines the interval, in milliseconds at which the internal timer triggers.
#define TIMER_TICK 100000

//STANDARD_ACTION_DELAY: defines the minimum time between relay acitons
#define STANDARD_ACTION_DELAY 300

//RUN TRACKER LOCATION: defines the location in EEPROM where the run log is stored
#define RUN_LOG_EEPROM_LOCATION 0

/*
	* Cycle Timing Parameters
	* The parameters listed below will alter the timing used for each portion of the wash cycle.
*/

//Run Parameter Definitions
const unsigned long fillFlowMeterCount = 950; //Pulses
const unsigned long rinseWashMotorRuntime = 300000; //Milli Seconds //300000
const unsigned long rinseDrainMotorRuntime = 55000; //Milli Seconds //55000
const unsigned long washTime = 1200000; //Milli Seconds -> About 20min
#ifdef SLOW_PWM_HEATER
	const unsigned long heatOnCycle = 3000; //Milli Seconds
	const unsigned long heatOffCycle = 6000; //Milli Seconds
#endif
const unsigned long dryTime = 300000; //Milli Seconds -> About 10min
#define DETERGENT_RELEASE_TIME 40000

/*
  * Cycle Feature Selection
  *
*/

#define STARTING_SMART_RINSE_V1 //Enables smart rinse at start of cycle

//
// BEHAVIOURAL PARAMETERS - Conditionals
//----------------------------------------------------------------------------------------------------

#endif

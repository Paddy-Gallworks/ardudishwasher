#include "StepMotor.h"

void StepMotor::begin(uint8_t _step, uint8_t _dir, uint8_t _enable)
{
	stepPin = _step;
	dirPin = _dir;
	enablePin = _enable;
	pinMode(stepPin,OUTPUT);
	pinMode(dirPin,OUTPUT);
	pinMode(enablePin,OUTPUT);
	digitalWrite(enablePin,HIGH);//disable by default
}

void StepMotor::step()
{
	digitalWrite(stepPin,HIGH);
	digitalWrite(stepPin,LOW);
}
void StepMotor::setDirDirect(MotorDirection _dir)
{
	if(_dir==POSITIVE)
	{
		digitalWrite(dirPin,HIGH);
	}
	else
	{
		digitalWrite(dirPin,LOW);
	}
}

void StepMotor::disable()
{
	digitalWrite(enablePin,HIGH);
}
void StepMotor::enable()
{
	digitalWrite(enablePin,LOW);
}
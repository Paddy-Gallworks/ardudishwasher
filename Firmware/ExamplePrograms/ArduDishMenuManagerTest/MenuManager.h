#include <Arduino.h>
#include <LiquidCrystal.h>
#include <string.h>
#include "Configuration.h"
#define MENU_SIZE 8

//Screen Parameters
#define LINE_COUNT 2
#define CHAR_COUNT 16

class MenuManager
{
  public:
  // * CONSTRUCTOR
  MenuManager();
  // * DATA
  LiquidCrystal lcd = LiquidCrystal(8, 9, 4, 5, 6, 7);
  void (*menuFunc[MENU_SIZE])(int);
  String functionDescriptor[MENU_SIZE];
  // Current state of the screen
  uint8_t menuIndex = 0;
  uint8_t cursorPosition = 0;
  // * FUNCTIONS
  void addMenuItem(uint8_t,String,void menuFunction(int));
  void updateScreen();
  void handleNavigationEvent(int);
};

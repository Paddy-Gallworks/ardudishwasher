/*
  * ARDUDISH Project
  "ArduDishMenuManagerTest.ino"
  [Desc]

  - By Patrick Gall
*/
#include "MenuManager.h"
#include "Configuration.h"
#include <LiquidCrystal.h>
//Variables used for rotary encoder.
uint8_t Encoder_A = 3;            // Incremental Encoder singal A is PD3
uint8_t Encoder_B = 2;            // Incremental Encoder singal B is PD2
//Variables used for hat key.
uint16_t adc_key_in  = 0;
uint8_t eventFlag = 0;
uint8_t encoderFlag = 0;
//LCD Object
LiquidCrystal lcd(8, 9, 4, 5, 6, 7);
//Menu Manager Object
MenuManager menu = MenuManager();
void Encoder_san() {
  if(digitalRead(Encoder_B)) {
    //Encoder_number++;
    encoderFlag = encoderUp;
  }
  else {
    //Encoder_number--;
    encoderFlag = encoderDown;
  }
}
uint8_t read_LCD_buttons() {
  adc_key_in = analogRead(0);
  if (adc_key_in > 1000) return btnNone;
  if (adc_key_in < 50)   return btnLeft;
  if (adc_key_in < 150)  return btnUp;
  if (adc_key_in < 250)  return btnRight;
  if (adc_key_in < 450)  return btnSelect;
  if (adc_key_in < 700)  return btnDown;
  if (adc_key_in < 850)  return btnEncodeOK;
  // For V1.0 comment the other threshold and use the one below:
  /*  if (adc_key_in < 50)   return btnRIGHT;
  if (adc_key_in < 195)  return btnUP;
  if (adc_key_in < 380)  return btnDOWN;
  if (adc_key_in < 555)  return btnLEFT;
  if (adc_key_in < 790)  return btnSELECT;
  */
  return btnNone;
}
void setup() {
  Serial.begin(9600);
  Serial.println("Arudish Menu Manager Test");
  //Start LCD
  lcd.begin(16, 2);
  pinMode(10,OUTPUT);
  digitalWrite(10, 1);
  lcd.setCursor(0,0);
  lcd.print("ArduDish!");
  delay(1000);
  //Start Inputs
  pinMode(Encoder_A, INPUT);
  pinMode(Encoder_B, INPUT);
  digitalWrite(Encoder_A, 1);
  digitalWrite(Encoder_B, 1);
  attachInterrupt(1, Encoder_san, FALLING); //interrupts: numbers 0 (on digital pin 2) and 1 (on digital pin 3).
  menu.addMenuItem(0,"F One",exampleFunctionOne);
  menu.addMenuItem(1,"F Two",exampleFunctionTwo);
  menu.addMenuItem(2,"Normal Wash",exampleWash);
  menu.updateScreen();
}
void loop() {
  eventFlag = read_LCD_buttons();
  if(eventFlag != btnNone) {
    menu.handleNavigationEvent(eventFlag);
    eventFlag = btnNone;
  }
  if(encoderFlag != 0) {
    menu.handleNavigationEvent(encoderFlag);
    encoderFlag = 0;
  }
}
void exampleFunctionOne() {
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("Function 1");
  delay(1000);
}
void exampleFunctionTwo() {
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("Function 2");
  delay(1000);
}
void exampleWash() {
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("Washing...");
  delay(1000);
}

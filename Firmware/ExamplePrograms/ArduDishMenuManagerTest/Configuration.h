#ifndef CONFIGURATION_H
#define CONFIGURATION_H
enum navigationEvents {
  btnRight, //0
  btnUp, //1
  btnDown, //2
  btnLeft, //3
  btnSelect, //4
  btnEncodeOK, //5
  encoderUp, //6
  encoderDown, //7
  btnNone //8
};
#endif

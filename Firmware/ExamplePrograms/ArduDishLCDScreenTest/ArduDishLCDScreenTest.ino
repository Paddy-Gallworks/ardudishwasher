/*
  * ARDUDISH Project
  {ArduDishLCDScreenTest.ino}
  [Desc]
  This code was adapter from the ElecFreaks demo code to test thier LCD Keypad Module.
  This is only useful if your Ardudish is using the modular control board.
  Origonal code was developed by Mark Bramwell in August 2013.
  - By Patrick Gall
*/

#include <LiquidCrystal.h>   
//Variables used for rotary encoder.
const int Encoder_A = 3;            // Incremental Encoder singal A is PD3 
const int Encoder_B = 2;            // Incremental Encoder singal B is PD2 
unsigned int Encoder_number = 0;
//Variables used for hat key.
int state = 0;
int lcd_key     = 0;
int adc_key_in  = 0;
#define btnRIGHT  0
#define btnUP     1 
#define btnDOWN   2 
#define btnLEFT   3
#define btnSELECT 4
#define btnNONE   5   
#define btnEncodeOK  6 
LiquidCrystal lcd(8, 9, 4, 5, 6, 7);  
void Encoder_san() {  
  if(digitalRead(Encoder_B)) {
    Encoder_number++;
  }
  else {  
    Encoder_number--;
  }     
  state=1;
}
int read_LCD_buttons() {  
  adc_key_in = analogRead(0);      
  if (adc_key_in > 1000) return btnNONE; 
  if (adc_key_in < 50)   return btnLEFT;   
  if (adc_key_in < 150)  return btnUP;   
  if (adc_key_in < 250)  return btnRIGHT;   
  if (adc_key_in < 450)  return btnSELECT;   
  if (adc_key_in < 700)  return btnDOWN;     
  if (adc_key_in < 850)  return btnEncodeOK;
  // For V1.0 comment the other threshold and use the one below: 
  /*  if (adc_key_in < 50)   return btnRIGHT;    
  if (adc_key_in < 195)  return btnUP;  
  if (adc_key_in < 380)  return btnDOWN;  
  if (adc_key_in < 555)  return btnLEFT;   
  if (adc_key_in < 790)  return btnSELECT;    
  */    
  return btnNONE;
}   
void setup() {  
  lcd.begin(16, 2);   // start the library  
  pinMode(10,OUTPUT);
  digitalWrite(10, 1);
  lcd.setCursor(0,0);  
  lcd.print("Push the buttons");
  pinMode(Encoder_A, INPUT); 
  pinMode(Encoder_B, INPUT); 
  digitalWrite(Encoder_A, 1);
  digitalWrite(Encoder_B, 1);
  attachInterrupt(1, Encoder_san, FALLING); //interrupts: numbers 0 (on digital pin 2) and 1 (on digital pin 3).
}   
void loop() {  
  if(state==1) {  
    lcd.clear();
    lcd.setCursor(9,1);            // move cursor to second line "1" and 9 spaces over  
    lcd.print(Encoder_number); 
    lcd.setCursor(0,0);            // move cursor to second line "1" and 9 spaces over  
    lcd.print("Push the buttons"); // print a simple message 
    state=0;
  } 
  lcd.setCursor(0,1);            // move to the begining of the second line  
  lcd_key = read_LCD_buttons();  // read the buttons    
  switch (lcd_key) {          // depending on which button was pushed, we perform an action  
    case btnRIGHT: {      
      lcd.print("RIGHT ");      
      break;     
    }   
    case btnLEFT: {    
      lcd.print("LEFT   ");  
      break;    
    }  
    case btnUP: {     
      lcd.print("UP    "); 
      break;     
    }   
    case btnDOWN: {     
      lcd.print("DOWN  ");     
      break;     
    }   
    case btnSELECT: {    
      lcd.print("SELECT");     
      break;    
    } 
    case btnEncodeOK: {     
      lcd.print("EncdOK");   
      break;     
    } 
    case btnNONE: {     
      lcd.print("NONE  ");   
      break;   
    } 
  } 
}   

//Variables
INSERTION_DEPTH = 5.5;
ENCODER_RADIUS = 3.05;
LOWER_KNOB_RADIUS = 5;
LOWER_KNOB_HEIGHT = 6;
LOWER_KNURL_RADIUS = 7.5;
UPPER_KNURL_RADIUS = 5;
KNURL_TAPER_HEIGHT = 4;
DIMPLE_RADIUS = 12;
DIMPLE_EFFECTIVE_HEIGHT = 12.2;

//Conditionals


//Model
union() { 
  difference() { //bottum of the knob inserted on encoder
    cylinder(r=LOWER_KNOB_RADIUS,h=LOWER_KNOB_HEIGHT);
    translate([0,0,-0.01])
      cylinder(r=ENCODER_RADIUS,h=INSERTION_DEPTH);
  }
  translate([0,0,LOWER_KNOB_HEIGHT-0.01]) //upper tapered section of knob
    difference() {
      hull() {
        cylinder(r=LOWER_KNURL_RADIUS,h=1);
        translate([0,0,KNURL_TAPER_HEIGHT])
          cylinder(r=UPPER_KNURL_RADIUS,h=1);
      }
      #translate([0,0,DIMPLE_EFFECTIVE_HEIGHT+KNURL_TAPER_HEIGHT])
        sphere(r=DIMPLE_RADIUS);
    }
}
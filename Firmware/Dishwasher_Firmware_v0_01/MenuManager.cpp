#include "MenuManager.h"

MenuManager::MenuManager() {
  //Construct the blank menu here!
  for(int i = 0;i<MENU_SIZE;i++)
  {
    functionDescriptor[i] = "BLANK" + (String)i;
  }
  lcd.begin(CHAR_COUNT,LINE_COUNT);
}

void MenuManager::updateScreen() {
  //Draw the current screen
  lcd.clear();
  if(screenDisplayMode == mainMenu) {
      //First Line
    lcd.setCursor(1,0);
    lcd.print(functionDescriptor[menuIndex]);
    Serial.println(functionDescriptor[menuIndex]);
      //Second Line
    lcd.setCursor(1,1);
    lcd.print(functionDescriptor[menuIndex+1]);
    Serial.println(functionDescriptor[menuIndex+1]);
      //Draw Cursor
    lcd.setCursor(0,cursorPosition);
    lcd.print("*");
  }
  else if(screenDisplayMode == infoScreen) { //Displays information for the cycle as it runs
    Serial.println("infoScreen");
    //Line 1: Run: Rinse
    //Line 2:
    lcd.setCursor(0,0);
    lcd.print("Run: " + currentFunction);
    lcd.setCursor(0,1);
    lcd.print(tickerMessage);
  }
  else if(screenDisplayMode == simpleFunction) {
    lcd.setCursor(0,0);
    lcd.print("Run: " + currentFunction);
  }
  else if(screenDisplayMode == startUp) {
    lcd.setCursor(0,0);
    lcd.print("ARDUDISH Loading");
    lcd.setCursor(0,1);
    lcd.print(tickerMessage);
  }
  else { //error catch all -> resets to mainMenu
    screenDisplayMode = mainMenu;
  }

}

void MenuManager::handleNavigationEvent(int eventFlag) {
  //Check the mode of the screenDisplayMode
  if(screenDisplayMode == mainMenu) {
    //Update the indeces based on an eventFlag
    Serial.print("Event: ");
    Serial.println(eventFlag);
    if(eventFlag == encoderUp) {
      if(cursorPosition == 0) {
        cursorPosition++;
      }
      else {
        if(menuIndex == MENU_SIZE-2) {
          menuIndex = 0;
          cursorPosition = 0;
        }
        else {
          menuIndex++;
        }
      }
    }
    else if(eventFlag == encoderDown) {
      if(cursorPosition == LINE_COUNT-1) {
        cursorPosition--;
      }
      else {
        if(menuIndex == 0) {
          menuIndex = MENU_SIZE-2;
          cursorPosition = LINE_COUNT-1;
        }
        else {
          menuIndex--;
          //cursorPosition = LINE_COUNT-1;
        }
      }
      Serial.print("Menu Index: ");
      Serial.println(menuIndex);
      Serial.print("Curosr Index: ");
      Serial.println(cursorPosition);
    }
    else if(eventFlag == btnEncodeOK) {
      Serial.print("Running: ");
      Serial.println(functionDescriptor[cursorPosition+menuIndex]);
      currentFunction = functionDescriptor[cursorPosition+menuIndex];
      menuFunc[cursorPosition + menuIndex](0);
    }
  }
  else if(screenDisplayMode == infoScreen) {

  }
  updateScreen();
}

void MenuManager::addMenuItem(uint8_t index,String text,void newMenuFunction(int)) {
  functionDescriptor[index] = text;
  menuFunc[index] = newMenuFunction;
}

void MenuManager::tickerDisplay(String message) {
  tickerMessage = message;
}

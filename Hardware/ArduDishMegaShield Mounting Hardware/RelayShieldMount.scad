$fn=30;
//Variables
BOLT_SPACING_WIDTH = 66.5;
BOLT_SPACING_HEIGHT = 92;
MOUNT_THICKNESS = 3;
OUTER_EDGE_THICKNESS = 12;
INSERT_HEIGHT = 6.5;
INSERT_RADIUS = 1.8;
INSERT_HUB_RADIUS = 4;

difference() {
  union() {
    minkowski() {
      cube([BOLT_SPACING_WIDTH,BOLT_SPACING_HEIGHT,MOUNT_THICKNESS]);
      cylinder(r=5,h=0.001);
    }
    translate([0,0,0])
      cylinder(r=INSERT_HUB_RADIUS,h=INSERT_HEIGHT);
    translate([0,92,0])
      cylinder(r=INSERT_HUB_RADIUS,h=INSERT_HEIGHT);
    translate([66.5,0,0])
      cylinder(r=INSERT_HUB_RADIUS,h=INSERT_HEIGHT);
    translate([66.5,92,0])
      cylinder(r=INSERT_HUB_RADIUS,h=INSERT_HEIGHT);
  }
  translate([OUTER_EDGE_THICKNESS,OUTER_EDGE_THICKNESS,-0.01])
  minkowski() {
    cube([BOLT_SPACING_WIDTH-(2*OUTER_EDGE_THICKNESS),BOLT_SPACING_HEIGHT-(2*OUTER_EDGE_THICKNESS),10]);
    cylinder(r=5,h=0.001);
  }
  translate([0,0,-0.01])
    cylinder(r=INSERT_RADIUS,h=10);
  translate([0,BOLT_SPACING_HEIGHT,-0.01])
    cylinder(r=INSERT_RADIUS,h=10);
  translate([BOLT_SPACING_WIDTH,0,-0.01])
    cylinder(r=INSERT_RADIUS,h=10);
  translate([BOLT_SPACING_WIDTH,BOLT_SPACING_HEIGHT,-0.01])
    cylinder(r=INSERT_RADIUS,h=10);
}
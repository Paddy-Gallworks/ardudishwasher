EESchema Schematic File Version 4
LIBS:ArduDishPowerDistributionBoard-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 3800 2750 0    50   ~ 0
AC Power Supply
$Comp
L power:GND #PWR01
U 1 1 5D91DD8A
P 3950 2350
F 0 "#PWR01" H 3950 2100 50  0001 C CNN
F 1 "GND" H 3955 2177 50  0000 C CNN
F 2 "" H 3950 2350 50  0001 C CNN
F 3 "" H 3950 2350 50  0001 C CNN
	1    3950 2350
	1    0    0    -1  
$EndComp
Wire Wire Line
	3400 2050 3500 2050
Wire Wire Line
	3500 2050 3500 2200
Wire Wire Line
	3500 2200 3950 2200
Wire Wire Line
	3950 2200 3950 2350
Wire Wire Line
	3950 2150 3950 2200
Connection ~ 3950 2200
Wire Wire Line
	2600 2050 2050 2050
Wire Wire Line
	2050 2050 2050 1950
Wire Wire Line
	2050 1950 1850 1950
$Comp
L Connector:Conn_01x02_Male J1
U 1 1 5D91DD99
P 1650 1850
F 0 "J1" H 1758 2031 50  0000 C CNN
F 1 "Conn_01x02_Male" H 1758 1940 50  0000 C CNN
F 2 "TerminalBlock_RND:TerminalBlock_RND_205-00067_1x02_P7.50mm_Horizontal" H 1650 1850 50  0001 C CNN
F 3 "~" H 1650 1850 50  0001 C CNN
	1    1650 1850
	1    0    0    -1  
$EndComp
$Comp
L power:+9V #PWR02
U 1 1 5D91DD9F
P 4350 1850
F 0 "#PWR02" H 4350 1700 50  0001 C CNN
F 1 "+9V" H 4365 2023 50  0000 C CNN
F 2 "" H 4350 1850 50  0001 C CNN
F 3 "" H 4350 1850 50  0001 C CNN
	1    4350 1850
	1    0    0    -1  
$EndComp
Wire Wire Line
	2400 1850 2600 1850
Wire Wire Line
	1850 1850 2100 1850
Wire Wire Line
	3500 1850 3400 1850
$Comp
L Device:Fuse F2
U 1 1 5D91DDB1
P 3650 1850
F 0 "F2" V 3453 1850 50  0000 C CNN
F 1 "350mA" V 3544 1850 50  0000 C CNN
F 2 "Fuse:Fuseholder_TR5_Littelfuse_No560_No460" V 3580 1850 50  0001 C CNN
F 3 "~" H 3650 1850 50  0001 C CNN
	1    3650 1850
	0    1    1    0   
$EndComp
$Comp
L Device:Fuse F1
U 1 1 5D91DDB7
P 2250 1850
F 0 "F1" V 2053 1850 50  0000 C CNN
F 1 "500mA" V 2144 1850 50  0000 C CNN
F 2 "Fuse:Fuseholder_TR5_Littelfuse_No560_No460" V 2180 1850 50  0001 C CNN
F 3 "~" H 2250 1850 50  0001 C CNN
	1    2250 1850
	0    1    1    0   
$EndComp
$Comp
L Converter_ACDC:IRM-03-5 PS1
U 1 1 5D91DDBD
P 3000 1950
F 0 "PS1" H 3000 2317 50  0000 C CNN
F 1 "IRM-03-5" H 3000 2226 50  0000 C CNN
F 2 "Converter_ACDC:Converter_ACDC_MeanWell_IRM-03-xx_THT" H 3000 1600 50  0001 C CNN
F 3 "https://www.meanwell.com/Upload/PDF/IRM-03/IRM-03-SPEC.PDF" H 3000 1550 50  0001 C CNN
	1    3000 1950
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Male J2
U 1 1 5D91FA80
P 5600 2000
F 0 "J2" H 5572 1882 50  0000 R CNN
F 1 "Conn_01x02_Male" H 5572 1973 50  0000 R CNN
F 2 "Connector_JST:JST_PH_S2B-PH-K_1x02_P2.00mm_Horizontal" H 5600 2000 50  0001 C CNN
F 3 "~" H 5600 2000 50  0001 C CNN
	1    5600 2000
	-1   0    0    1   
$EndComp
$Comp
L power:+9V #PWR03
U 1 1 5D920F2A
P 5200 1800
F 0 "#PWR03" H 5200 1650 50  0001 C CNN
F 1 "+9V" H 5215 1973 50  0000 C CNN
F 2 "" H 5200 1800 50  0001 C CNN
F 3 "" H 5200 1800 50  0001 C CNN
	1    5200 1800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR04
U 1 1 5D9215AA
P 5200 2150
F 0 "#PWR04" H 5200 1900 50  0001 C CNN
F 1 "GND" H 5205 1977 50  0000 C CNN
F 2 "" H 5200 2150 50  0001 C CNN
F 3 "" H 5200 2150 50  0001 C CNN
	1    5200 2150
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 2150 5200 2000
Wire Wire Line
	5200 2000 5400 2000
Wire Wire Line
	5400 1900 5200 1900
Wire Wire Line
	5200 1900 5200 1800
$Comp
L Device:CP1 C1
U 1 1 5D9251CC
P 3950 2000
F 0 "C1" H 4065 2046 50  0000 L CNN
F 1 "470uF" H 4065 1955 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_10x10" H 3950 2000 50  0001 C CNN
F 3 "~" H 3950 2000 50  0001 C CNN
	1    3950 2000
	1    0    0    -1  
$EndComp
Wire Wire Line
	3800 1850 3950 1850
Connection ~ 3950 1850
Wire Wire Line
	3950 1850 4350 1850
$EndSCHEMATC
